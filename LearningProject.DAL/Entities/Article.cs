﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearningProject.DAL.Entities
{
    public class Article
    {
        [Key]
        public int Id { get; set; }

        public string Author { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImgPath { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
