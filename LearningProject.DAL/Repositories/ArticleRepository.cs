﻿using LearningProject.DAL.Context;
using LearningProject.DAL.Entities;
using LearningProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LearningProject.DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private ApplicationContext db;

        public ArticleRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public IEnumerable<Article> GetAll()
        {
            return db.Articles;
        }

        public Article Get(int id)
        {
            return db.Articles.Find(id);
        }

        public Article Get(string name)
        {
            return db.Articles.FirstOrDefault(a => a.Title == name);
        }

        public Article Get(string p1, string p2)
        {
            return db.Articles.FirstOrDefault(a => a.Title == p1 && a.Author == p2);
        }

        public void Create(Article article)
        {
            db.Articles.Add(article);
        }

        public void Update(Article article)
        {
            db.Entry(article).State = EntityState.Modified;
        }


        public IEnumerable<Article> Find(Func<Article, Boolean> predicate)
        {
            return db.Articles.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            Article article = db.Articles.Find(id);
            if (article != null)
                db.Articles.Remove(article);
        }
    }
}
