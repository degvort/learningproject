﻿using LearningProject.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace LearningProject.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> Users { get; }
        IRepository<Article> Articles { get; }
        void Save();
    }
}
