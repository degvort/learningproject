﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using LearningProject.DAL.Entities;
using Microsoft.EntityFrameworkCore.Internal;

namespace LearningProject.DAL.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Article> Articles { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public static void Initialize(ApplicationContext context)
        {
            if (!context.Users.Any())
            {
                context.Users.Add(
                    new User
                    {
                        Username = "admin",
                        Password = "admin",
                        Role = "Administrator"
                    });
                context.SaveChanges();
            }

            if (!context.Articles.Any())
            {
                context.Articles.AddRange(
                    new Article
                    {
                        Author = "Fictional Character",
                        Title = "The eloquent title",
                        Description = "This block needs to be filled in to see whether " +
                                      "the field is being filled correctly. I could simply" +
                                      " copy the article from the Internet, but I decided to " +
                                      "just throw a text that does not carry any informational" +
                                      " value. Hope you did not read this text until the end.",
                        CreateTime = DateTime.Now,
                        ImgPath = "http:\\\\localhost:51806\\StaticFiles\\Images\\profile.jpg"
                    },
                    new Article
                    {
                        Author = "Fictional Character",
                        Title = "The eloquent title",
                        Description = "This block needs to be filled in to see whether " +
                                      "the field is being filled correctly. I could simply" +
                                      " copy the article from the Internet, but I decided to " +
                                      "just throw a text that does not carry any informational" +
                                      " value. Hope you did not read this text until the end.",
                        CreateTime = DateTime.Now,
                        ImgPath = "http:\\\\localhost:51806\\StaticFiles\\Images\\profile.jpg"
                    });
                context.SaveChanges();
            }
        }
    }
}
