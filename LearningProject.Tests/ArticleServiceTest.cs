﻿using LearningProject.BLL.BusinessModels;
using LearningProject.BLL.DTO;
using LearningProject.BLL.Infrastructure;
using LearningProject.BLL.Services;
using LearningProject.DAL.Entities;
using LearningProject.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LearningProject.Tests
{
    public class ArticleServiceTest
    {
        [Fact]
        public void WhenArticleCreate_ThrowException()
        {
            //Arrange
            ArticleCreateDTO article = new ArticleCreateDTO();
            var uow = new Mock<IUnitOfWork>();
            var config = new Mock<IConfiguration>();

            var service = new ArticleService(uow.Object, config.Object);

            //Act
            var ex = Assert.Throws<ValidationException>(() => service.CreateArticle(article));

            //Assert
            Assert.Equal("INVALID_ARTICLE_DATA", ex.Message);
        }

        [Fact]
        public void WhenGetArticles_ReturnCorrectItems()
        {
            //Arrange
            ArticleCreateDTO article = new ArticleCreateDTO();
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(uowService => uowService.Articles.GetAll()).Returns(GetTestArticles());
            var config = new Mock<IConfiguration>();

            var service = new ArticleService(uow.Object, config.Object);

            var expected = new IndexModel<ArticleDTO>
            {
                PageModel = new PageModel(6, 1, 6),
                Data = new ArticleDTO[] 
                {
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ }
                }           
            };

            //Act
            var result = service.GetArticles(1);

            //Assert
            Assert.IsType<IndexModel<ArticleDTO>>(result);
            Assert.Equal(expected.Data.Count(), result.Data.Count());
            Assert.Equal(expected.PageModel.HasNextPage, result.PageModel.HasNextPage);
        }
        private IEnumerable<Article> GetTestArticles()
        {
            Article[] result = 
            {
                    new Article{ },
                    new Article{ },
                    new Article{ },
                    new Article{ },
                    new Article{ },
                    new Article{ }
            };
            return result;
        }
    }
}
