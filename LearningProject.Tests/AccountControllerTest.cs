﻿using LearningProject.BLL.BusinessModels;
using LearningProject.BLL.DTO;
using LearningProject.BLL.Infrastructure;
using LearningProject.BLL.Interfaces;
using LearningProject.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LearningProject.Tests
{
    public class AccountControllerTest
    {
        [Fact]
        public void Post_WhenUserLogin_GetOkResultObject()
        {
            //Arrange
            LoginModelDTO loginDto = new LoginModelDTO
            {
                Username = "username",
                Password = "password",
            };
            var userService = new Mock<IUserService>();
            userService.Setup(service => service.LoginUser(loginDto)).Returns(LoginUser());

            var localization = new Mock<IStringLocalizer<AccountController>>();

            var controller = new AccountController(userService.Object, localization.Object);

            //Act
            var result = controller.Login(loginDto);

            //Assert
            var response = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<LoginModelDTO>(response.Value);
            Assert.Equal(0, model.Id);
            Assert.Equal("username", model.Username);
            Assert.Equal("password", model.Password);
            Assert.Equal("role", model.Role);
            Assert.Equal("token", model.Token);
        }
        private LoginModelDTO LoginUser()
        {
            return new LoginModelDTO
            {
                Id = 0,
                Username = "username",
                Password = "password",
                Role = "role",
                Token = "token"
            };
        }

        [Fact]
        public void Post_WhenUserLogin_GetBadRequest()
        {
            //Arrange
            LoginModelDTO loginDto = new LoginModelDTO();
            var userService = new Mock<IUserService>();
            userService.Setup(service => service.LoginUser(loginDto)).Throws(new ValidationException("error", ""));

            var localization = new Mock<IStringLocalizer<AccountController>>();
            string key = "error";
            var localizedString = new LocalizedString(key, key);
            localization.Setup(_ => _[key]).Returns(localizedString);

            var controller = new AccountController(userService.Object, localization.Object);

            //Act
            var result = controller.Login(loginDto);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Post_WhenUserRegistrate_GetOkObjectResult()
        {
            //Arrange
            RegisterModelDTO registerDto = new RegisterModelDTO();
            var userService = new Mock<IUserService>();
            userService.Setup(service => service.RegisterUser(registerDto));

            var localization = new Mock<IStringLocalizer<AccountController>>();

            var controller = new AccountController(userService.Object, localization.Object);

            //Act
            var result = controller.Register(registerDto);

            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void Post_WhenUserRegistrate_GetBadRequest()
        {
            //Arrange
            RegisterModelDTO registerDto = new RegisterModelDTO();
            var userService = new Mock<IUserService>();
            userService.Setup(service => service.RegisterUser(registerDto)).Throws(new ValidationException("error", ""));

            var localization = new Mock<IStringLocalizer<AccountController>>();
            string key = "error";
            var localizedString = new LocalizedString(key, key);
            localization.Setup(_ => _[key]).Returns(localizedString);

            var controller = new AccountController(userService.Object, localization.Object);

            //Act
            var result = controller.Register(registerDto);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Get_WhenGetUsers_ReturnAllItems()
        {
            //Arrange
            var localization = new Mock<IStringLocalizer<AccountController>>();

            var userService = new Mock<IUserService>();
            userService.Setup(service => service.GetUsers(1)).Returns(GetTestUsers());

            var controller = new AccountController(userService.Object, localization.Object);

            //Act
            var result = controller.GetUsers(1);

            //Assert
            var response = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<IndexModel<UserDTO>>(response.Value);
            Assert.Equal(GetTestUsers().Data.Count(), model.Data.Count());
        }
        private IndexModel<UserDTO> GetTestUsers()
        {
            IndexModel<UserDTO> result = new IndexModel<UserDTO>
            {
                Data = new List<UserDTO>
                {
                    new UserDTO{ },
                    new UserDTO{ },
                    new UserDTO{ },
                    new UserDTO{ }
                },
            };
            return result;
        }

        [Fact]
        public void Post_WhenUserModify_GetOkObjectResult()
        {
            //Arrange
            UserDTO userDto = new UserDTO();
            var userService = new Mock<IUserService>();
            userService.Setup(service => service.UpdateUser(userDto));

            var localization = new Mock<IStringLocalizer<AccountController>>();

            var controller = new AccountController(userService.Object, localization.Object);

            //Act
            var result = controller.Modify(userDto);

            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void Post_WhenUserModify_GetBadObjectResult()
        {
            //Arrange
            UserDTO userDto = new UserDTO();
            var userService = new Mock<IUserService>();
            userService.Setup(service => service.UpdateUser(userDto)).Throws(new ValidationException("error", ""));

            var localization = new Mock<IStringLocalizer<AccountController>>();
            string key = "error";
            var localizedString = new LocalizedString(key, key);
            localization.Setup(_ => _[key]).Returns(localizedString);

            var controller = new AccountController(userService.Object, localization.Object);

            //Act
            var result = controller.Modify(userDto);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
