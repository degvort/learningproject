﻿using LearningProject.BLL.BusinessModels;
using LearningProject.BLL.DTO;
using LearningProject.BLL.Infrastructure;
using LearningProject.BLL.Interfaces;
using LearningProject.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LearningProject.Tests
{
    public class DashboardControllerTest
    {
        [Fact]
        public void Get_WhenGetArticles_ReturnAllItems()
        {
            //Arrange
            var localization = new Mock<IStringLocalizer<DashboardController>>();
            var articleService = new Mock<IArticleService>();
            articleService.Setup(service => service.GetArticles(1)).Returns(GetTestArticles());
            var controller = new DashboardController(articleService.Object, localization.Object);

            //Act
            var result = controller.Index();

            //Assert
            var response = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<IndexModel<ArticleDTO>>(response.Value);
            Assert.Equal(GetTestArticles().Data.Count(), model.Data.Count());
        }
        private IndexModel<ArticleDTO> GetTestArticles()
        {
            IndexModel<ArticleDTO> result = new IndexModel<ArticleDTO>
            {
                Data = new List<ArticleDTO>
                {
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ },
                    new ArticleDTO{ }
                },
            };
            return result;
        }

        [Fact]
        public void Post_WhenCreateArticle_GetBadRequestResult()
        {
            //Arrange
            ArticleCreateDTO articleDto = new ArticleCreateDTO();

            var localization = new Mock<IStringLocalizer<DashboardController>>();
            string key = "error";
            var localizedString = new LocalizedString(key, key);
            localization.Setup(_ => _[key]).Returns(localizedString);

            var articleService = new Mock<IArticleService>();
            articleService.Setup(service => service.CreateArticle(articleDto)).Throws(new ValidationException("error", ""));

            var controller = new DashboardController(articleService.Object, localization.Object);

            //Act
            var result = controller.CreateArticle(articleDto);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Post_WhenCreateArticle_OkResult()
        {
            //Arrange
            ArticleCreateDTO articleDto = new ArticleCreateDTO();

            var localization = new Mock<IStringLocalizer<DashboardController>>();
            var articleService = new Mock<IArticleService>();
            articleService.Setup(service => service.CreateArticle(articleDto));

            var controller = new DashboardController(articleService.Object, localization.Object);

            //Act
            var result = controller.CreateArticle(articleDto);

            //Assert
            Assert.IsType<OkResult>(result);
        }
    }
}
