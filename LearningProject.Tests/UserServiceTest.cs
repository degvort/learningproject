﻿using LearningProject.BLL.BusinessModels;
using LearningProject.BLL.DTO;
using LearningProject.BLL.Infrastructure;
using LearningProject.BLL.Services;
using LearningProject.DAL.Entities;
using LearningProject.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LearningProject.Tests
{
    public class UserServiceTest
    {
        [Fact]
        public void WhenLoginUser_ThrowException()
        {
            //Arrange
            LoginModelDTO loginDto = new LoginModelDTO();
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(_uow => _uow.Users.Get(loginDto.Username, loginDto.Password)).Returns(null as User);
            var config = new Mock<IConfiguration>();

            var service = new UserService(uow.Object, config.Object);

            //Act
            var ex = Assert.Throws<ValidationException>(() => service.LoginUser(loginDto));

            //Assert
            Assert.Equal("INVALID_LOGIN_INPUT", ex.Message);
        }
        
        [Fact]
        public void WhenLoginUser_ReturnCorrectObject()
        {
            //Arrange
            LoginModelDTO loginDto = new LoginModelDTO();
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(_uow => _uow.Users.Get(loginDto.Username, loginDto.Password)).Returns(GenerateUser);
            var config = new Mock<IConfiguration>();
            config.Setup(_ => _["Jwt:Key"]).Returns("This_is_super_secret_key_for_my_learning_project");
            config.Setup(_ => _["Jwt:Issuer"]).Returns("issuer");
            config.Setup(_ => _["Jwt:Audience"]).Returns("audience");

            var service = new UserService(uow.Object, config.Object);

            var expected = new LoginModelDTO
            {
                Id = 0,
                Username = "username",
                Password = "password",
                Role = "role",
                Token = JwtToken.GenerateJsonWebToken(GenerateUser(), "This_is_super_secret_key_for_my_learning_project", "issuer", "audience")
            };

            //Act
            var result = service.LoginUser(loginDto);

            //Assert
            Assert.IsAssignableFrom<LoginModelDTO>(result);
            Assert.Equal(expected.Id, result.Id);
            Assert.Equal(expected.Username, result.Username);
            Assert.Equal(expected.Token, result.Token);
        }
        private User GenerateUser()
        {
            return new User
            {
                Id = 0,
                Password = "password",
                Username = "username",
                Role = "role"
            };
        }
        
        [Fact]
        public void WhenRegisterUser_ThrowInvalidUsernameException()
        {
            //Arrange
            RegisterModelDTO registerDto = new RegisterModelDTO();
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(_uow => _uow.Users.Get(registerDto.Username)).Returns(new User());
            var config = new Mock<IConfiguration>();
            var service = new UserService(uow.Object, config.Object);

            //Act
            var ex = Assert.Throws<ValidationException>(() => service.RegisterUser(registerDto));

            //Assert
            Assert.Equal("USERNAME_ALREADY_USE", ex.Message);
        }
        
        [Fact]
        public void WhenRegisterUser_ThrowInvalidPasswordException()
        {
            //Arrange
            RegisterModelDTO registerDto = new RegisterModelDTO
            {
                Username = "username",
                Password = "password"
            };
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(_uow => _uow.Users.Get(registerDto.Username)).Returns(null as User);
            var config = new Mock<IConfiguration>();
            var service = new UserService(uow.Object, config.Object);

            //Act
            var ex = Assert.Throws<ValidationException>(() => service.RegisterUser(registerDto));

            //Assert
            Assert.Equal("INVALID_PASSWORD_INPUT", ex.Message);
        }
        
        [Fact]
        public void WhenUpdateUser_ThrowUserNotFoundException()
        {
            //Arrange
            UserDTO user = new UserDTO();
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(_uow => _uow.Users.Get(user.Id)).Returns(null as User);
            var config = new Mock<IConfiguration>();
            var service = new UserService(uow.Object, config.Object);

            //Act
            var ex = Assert.Throws<ValidationException>(() => service.UpdateUser(user));

            //Assert
            Assert.Equal("USER_NOT_FOUND", ex.Message);
        }
        
        [Fact]
        public void WhenGetUser_ThrowUserIdNotFoundException()
        {
            //Arrange
            var uow = new Mock<IUnitOfWork>();
            var config = new Mock<IConfiguration>();
            var service = new UserService(uow.Object, config.Object);

            //Act
            var ex = Assert.Throws<ValidationException>(() => service.GetUser(null));

            //Assert
            Assert.Equal("USER_ID_NOT_FOUND", ex.Message);
        }

        [Fact]
        public void WhenGetUsers_ThrowUserNotFoundException()
        {
            //Arrange
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(_uow => _uow.Users.Get(1)).Returns(null as User);
            var config = new Mock<IConfiguration>();
            var service = new UserService(uow.Object, config.Object);

            //Act
            var ex = Assert.Throws<ValidationException>(() => service.GetUser(1));

            //Assert
            Assert.Equal("USER_NOT_FOUND", ex.Message);
        }

        [Fact]
        public void WhenGetUsers_GetCorrectObject()
        {
            //Arrange
            var uow = new Mock<IUnitOfWork>();
            uow.Setup(_uow => _uow.Users.GetAll()).Returns(GetUsers());
            var config = new Mock<IConfiguration>();
            var service = new UserService(uow.Object, config.Object);

            var expected = new IndexModel<UserDTO>
            {
                PageModel = new PageModel(6,1,10),
                Data = new UserDTO[]
                {
                    new UserDTO(),
                    new UserDTO(),
                    new UserDTO(),
                    new UserDTO(),
                    new UserDTO(),
                    new UserDTO()
                }
            };

            //Act
            var result = service.GetUsers(1);

            //Assert
            Assert.IsAssignableFrom<IndexModel<UserDTO>>(result);
            Assert.Equal(expected.Data.Count(), result.Data.Count());
            Assert.Equal(expected.PageModel.HasNextPage, result.PageModel.HasNextPage);

        }
        private IEnumerable<User> GetUsers()
        {
            User[] users =
            {
                new User(),
                new User(),
                new User(),
                new User(),
                new User(),
                new User()
            };
            return users;
        }
    }
}