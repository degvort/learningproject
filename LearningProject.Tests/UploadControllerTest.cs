﻿using LearningProject.Controllers;
using LearningProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Primitives;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace LearningProject.Tests
{
    public class UploadControllerTest
    {
        [Fact]
        public void Post_WhenFileUpload_GetFileLenghtLessThenZeroResult()
        {
            //Arrange
            var localization = new Mock<IStringLocalizer<UploadController>>();
            string key = "FILE_LENGHT_EXP";
            var localizedString = new LocalizedString(key, key);
            localization.Setup(_ => _[key]).Returns(localizedString);
            var config = new Mock<IConfiguration>();
            config.Setup(x => x["Image:folderName"]).Returns("StaticFiles//Images");
            var controller = new UploadController(config.Object, localization.Object)
            {
                ControllerContext = RequestWithZeroLenghtFile()
            };

            //Act
            var result = controller.Upload();

            //Assert
            var response = Assert.IsType<BadRequestObjectResult>(result);
            var model = Assert.IsAssignableFrom<String>(response.Value);
            Assert.Equal("FILE_LENGHT_EXP", model);
        }
        private ControllerContext RequestWithZeroLenghtFile()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers.Add("Content-Type", "multipart/form-data");
            var file = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("This is a dummy file")), 0, 0, "Data", "dummy.txt");
            httpContext.Request.Form = new FormCollection(new Dictionary<string, StringValues>(), new FormFileCollection { file });
            var actx = new ActionContext(httpContext, new RouteData(), new ControllerActionDescriptor());
            return new ControllerContext(actx);
        }

        [Fact]
        public void Post_WhenFileUpload_GetNullReferenceExp()
        {
            //Arrange
            var localization = new Mock<IStringLocalizer<UploadController>>();
            string key = "NULL_REF_EXC";
            var localizedString = new LocalizedString(key, key);
            localization.Setup(_ => _[key]).Returns(localizedString);
            var config = new Mock<IConfiguration>();
            config.Setup(x => x["Image:folderName"]).Returns("StaticFiles//Images");
            var controller = new UploadController(config.Object, localization.Object)
            {
                ControllerContext = RequestWithNullFile()
            };

            //Act
            var result = controller.Upload();

            //Assert
            var response = Assert.IsType<BadRequestObjectResult>(result);
            var model = Assert.IsAssignableFrom<String>(response.Value);
            Assert.Equal("NULL_REF_EXC", model);
        }
        private ControllerContext RequestWithNullFile()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers.Add("Content-Type", "multipart/form-data");
            FormFile file = null;
            httpContext.Request.Form = new FormCollection(new Dictionary<string, StringValues>(), new FormFileCollection { file });
            var actx = new ActionContext(httpContext, new RouteData(), new ControllerActionDescriptor());
            return new ControllerContext(actx);
        }

        [Fact]
        public void Post_WhenFileUpload_GetOkObjectResult()
        {
            //Arrange
            var localization = new Mock<IStringLocalizer<UploadController>>();
            var config = new Mock<IConfiguration>();
            config.Setup(x => x["Image:folderName"]).Returns("");
            var controller = new UploadController(config.Object, localization.Object)
            {
                ControllerContext = RequestWithOkFile()
            };

            //Act
            var result = controller.Upload();

            //Assert
            var response = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<String>(response.Value);
        }
        private ControllerContext RequestWithOkFile()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers.Add("Content-Type", "multipart/form-data");
            var file = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("This is a dummy file")), 0, 4, "Data", "dummy.txt")
            {
                Headers = new HeaderDictionary(),
                ContentDisposition = "form-data; name=\"Data\"; filename=\"dummy.txt\"",
                ContentType = "text/txt"
            };
            httpContext.Request.Form = new FormCollection(new Dictionary<string, StringValues>(), new FormFileCollection { file });
            var actx = new ActionContext(httpContext, new RouteData(), new ControllerActionDescriptor());
            return new ControllerContext(actx);
        }
    }
}
