﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningProject.Models
{
    public class Error
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public Error(int code, string message)
        {
            StatusCode = code;
            Message = message;
        }

    }
}
