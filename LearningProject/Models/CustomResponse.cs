﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningProject.Models
{
    public class CustomResponse
    {
        public object Result { get; set; }
        public Error Error;

        public CustomResponse(object obj, int code)
        {
            if (code == 200)
            {
                Result = obj;
            }
            else
            {
               Error = new Error(0, obj.ToString());
            }
        }
    }
}
