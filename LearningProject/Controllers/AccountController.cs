﻿using LearningProject.BLL.DTO;
using LearningProject.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;


namespace LearningProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IStringLocalizer<AccountController> _localizer;
        private IUserService _userService;

        public AccountController(IUserService serv, IStringLocalizer<AccountController> localizer)
        {
            _localizer = localizer;
            _userService = serv;
        }

        [HttpPost]
        [Route("/login")]
        public IActionResult Login(LoginModelDTO model)
        {
            try
            {
                var loginDto = _userService.LoginUser(model);

                return Ok(loginDto);
            }
            catch(Exception ex)
            {
                return BadRequest(_localizer[ex.Message].Value);
            }
}

        [HttpPost]
        [Route("/register")]
        public IActionResult Register(RegisterModelDTO model)
        {
            try
            {
                _userService.RegisterUser(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(_localizer[ex.Message].Value);
            }
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        [Route("/users")]
        public IActionResult GetUsers(int page = 1)
        {
            return Ok(_userService.GetUsers(page));
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [Route("/modify")]
        public IActionResult Modify(UserDTO userDto)
        {
            try
            {
                _userService.UpdateUser(userDto);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(_localizer[ex.Message].Value);
            }
        }
    }
}