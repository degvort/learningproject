﻿using LearningProject.BLL.DTO;
using LearningProject.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;

namespace LearningProject.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : Controller
    {
        private readonly IStringLocalizer<DashboardController> _localizer;
        private IArticleService _articleService;

        public DashboardController(IArticleService serv, IStringLocalizer<DashboardController> localizer)
        {
            _localizer = localizer;
            _articleService = serv;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Index(int page=1)
        {
            return Ok(_articleService.GetArticles(page));
        }

        [Route("/createArticle")]
        [HttpPost]
        public IActionResult CreateArticle(ArticleCreateDTO article)
        {
            try
            {
                _articleService.CreateArticle(article);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(_localizer[ex.Message].Value);
            }
        }
    }
}