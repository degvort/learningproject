﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System;
using System.IO;
using System.Net.Http.Headers;

namespace LearningProject.Controllers
{
    [ApiController]
    public class UploadController : Controller
    {
        private readonly IConfiguration _config;
        private readonly IStringLocalizer<UploadController> _localizer;

        public UploadController(IConfiguration config, IStringLocalizer<UploadController> localizer)
        {
            _config = config;
            _localizer = localizer;
        }
        [Route("/upload")]
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), _config["Image:folderName"]);

                if (file.Length > 0)
                {
                    var newFileName = GenerateNewFileName(file);
                    var fullPath = Path.Combine(pathToSave, newFileName);
                    var dbPath = Path.Combine(_config["Image:folderName"], newFileName);

                    SaveFile(file, fullPath);
                    return Ok(dbPath);
                }
                else
                {
                    return BadRequest(_localizer["FILE_LENGHT_EXP"].Value);
                }
            }
            catch (NullReferenceException)
            {
                return BadRequest(_localizer["NULL_REF_EXC"].Value);
            }
        }
        private string GenerateNewFileName(IFormFile file)
        {
            var myUniqueFileName = Convert.ToString(Guid.NewGuid());
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            var fileExtension = Path.GetExtension(fileName);
            return myUniqueFileName + fileExtension;
        }
        private void SaveFile(IFormFile file, string path)
        {
            using (var stream = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(stream);
            }
        }
    }
}