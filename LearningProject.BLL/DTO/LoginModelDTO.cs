﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LearningProject.BLL.DTO
{
    public class LoginModelDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "The field can not be empty!")]
        public string Username { get; set; }
        [Required(ErrorMessage = "The field can not be empty!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
    }
}
