﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LearningProject.BLL.DTO
{
    public class ArticleCreateDTO
    {
        [Required(ErrorMessage = "The field can not be empty!")]
        public string Author { get; set; }
        [Required(ErrorMessage = "The field can not be empty!")]
        public string Title { get; set; }
        [Required(ErrorMessage = "The field can not be empty!")]
        public string Description { get; set; }
        public string ImgPath { get; set; }
    }
}
