﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LearningProject.BLL.DTO
{
    public class RegisterModelDTO
    {
        [Required(ErrorMessage = "The field can not be empty!")]
        [RegularExpression(@"^[A-Za-z0-9]+$", ErrorMessage = "Email can consist only letters and numbers")]
        public string Username { get; set; }
        [Required(ErrorMessage = "The field can not be empty!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match!")]
        public string ConfirmPassword { get; set; }
    }
}
