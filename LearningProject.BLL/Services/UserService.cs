﻿using AutoMapper;
using LearningProject.BLL.BusinessModels;
using LearningProject.BLL.DTO;
using LearningProject.BLL.Infrastructure;
using LearningProject.BLL.Interfaces;
using LearningProject.DAL.Entities;
using LearningProject.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace LearningProject.BLL.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork _uow { get; set; }
        private readonly IConfiguration _config;

        public UserService(IUnitOfWork uow, IConfiguration config)
        {
            _uow = uow;
            _config = config;
        }
        public LoginModelDTO LoginUser(LoginModelDTO loginDto)
        {
            User user = _uow.Users.Get(loginDto.Username, loginDto.Password);
            if (user == null)
                throw new ValidationException("INVALID_LOGIN_INPUT", "");

            return new LoginModelDTO {
                Id = user.Id,
                Username = user.Username,
                Password = user.Password,
                Role = user.Role,
                Token = JwtToken.GenerateJsonWebToken(user, _config["Jwt:Key"], _config["Jwt:Issuer"], _config["Jwt:Audience"])
            };
        }

        public void RegisterUser(RegisterModelDTO registerDto)
        {
            User user = _uow.Users.Get(registerDto.Username);
            if (user == null)
            {
                if (CheckPassword(registerDto.Password))
                {
                    _uow.Users.Create(new User
                    {
                        Username = registerDto.Username,
                        Password = registerDto.Password,
                        Role = "Guest"
                    });
                    _uow.Save();
                }
                else
                {
                    throw new ValidationException("INVALID_PASSWORD_INPUT", "");
                }
            }
            else
                throw new ValidationException("USERNAME_ALREADY_USE", "");
        }

        public void UpdateUser(UserDTO userDto)
        {
            User user = _uow.Users.Get(userDto.Id);
            if (user == null)
                throw new ValidationException("USER_NOT_FOUND", "");
            else
            {
                user.Username = userDto.Username;
                user.Role = userDto.Role;
                _uow.Users.Update(user);
                _uow.Save();
            }
        }

        public UserDTO GetUser(int? id)
        {
            if (id == null)
                throw new ValidationException("USER_ID_NOT_FOUND", "");
            var user = _uow.Users.Get(id.Value);
            if(user == null)
                throw new ValidationException("USER_NOT_FOUND", "");

            return new UserDTO
            {
                Id = user.Id,
                Username = user.Username,
                Password = user.Password,
                Role = user.Role
            };
        }

        public IndexModel<UserDTO> GetUsers(int page)
        {
            int pageSize = 10;

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>()).CreateMapper();
            IEnumerable<UserDTO> source = mapper.Map<IEnumerable<User>, List<UserDTO>>(_uow.Users.GetAll());

            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToArray();

            PageModel pageModel = new PageModel(count, page, pageSize);
            IndexModel<UserDTO> viewModel = new IndexModel<UserDTO>
            {
                PageModel = pageModel,
                Data = items
            };

            return viewModel;
        }

        public void Dispose()
        {
            _uow.Dispose();
        }

        private bool CheckPassword(string input)
        {
            int tmp = 0;
            if (Regex.IsMatch(input, @"^[A-Za-z0-9]+$"))
            {
                foreach (var c in input)
                {
                    if (Char.IsDigit(c))
                        tmp++;
                }
                if (tmp == 1)
                    return true;
            }
            return false;
        }
    }
}
