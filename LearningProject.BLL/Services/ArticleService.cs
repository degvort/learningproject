﻿using AutoMapper;
using LearningProject.BLL.BusinessModels;
using LearningProject.BLL.DTO;
using LearningProject.BLL.Infrastructure;
using LearningProject.BLL.Interfaces;
using LearningProject.DAL.Entities;
using LearningProject.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LearningProject.BLL.Services
{
    public class ArticleService : IArticleService
    {
        IUnitOfWork _uow { get; set; }
        IConfiguration _config;

        public ArticleService(IUnitOfWork uow, IConfiguration config)
        {
            _uow = uow;
            _config = config;
        }

        public void CreateArticle(ArticleCreateDTO articleDto)
        {
            if (articleDto.Author == null || articleDto.Description == null || articleDto.Title == null)
                throw new ValidationException("INVALID_ARTICLE_DATA", "");
            _uow.Articles.Create(new Article
            {
                Author = articleDto.Author,
                Title = articleDto.Title,
                Description = articleDto.Description,
                ImgPath = _config["Image:serverName"] + articleDto.ImgPath,
                CreateTime = DateTime.Now
            });
            _uow.Save();
        }

        public IndexModel<ArticleDTO> GetArticles(int page)
        {
            int pageSize = 6;

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Article, ArticleDTO>()).CreateMapper();
            IEnumerable<ArticleDTO> source = mapper.Map<IEnumerable<Article>, List<ArticleDTO>>(_uow.Articles.GetAll().Reverse());

            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToArray();

            PageModel pageModel = new PageModel(count, page, pageSize);
            IndexModel<ArticleDTO> viewModel = new IndexModel<ArticleDTO>
            {
                PageModel = pageModel,
                Data = items
            };

            return viewModel;
        }

        public void Dispose()
        {
            _uow.Dispose();
        }
    }
}
