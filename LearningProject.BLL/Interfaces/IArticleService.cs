﻿using LearningProject.BLL.DTO;
using LearningProject.BLL.BusinessModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace LearningProject.BLL.Interfaces
{
    public interface IArticleService
    {
        void CreateArticle(ArticleCreateDTO articleDto);
        IndexModel<ArticleDTO> GetArticles(int page);
        void Dispose();
    }
}
