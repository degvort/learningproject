﻿using System;
using System.Collections.Generic;
using System.Text;
using LearningProject.BLL.DTO;
using LearningProject.BLL.BusinessModels;
using LearningProject.DAL.Entities;

namespace LearningProject.BLL.Interfaces
{
    public interface IUserService
    {
        LoginModelDTO LoginUser(LoginModelDTO loginDto);
        void RegisterUser(RegisterModelDTO registerDto);
        void UpdateUser(UserDTO userDto);
        UserDTO GetUser(int? id);
        IndexModel<UserDTO> GetUsers(int page);
        void Dispose();
    }
}
