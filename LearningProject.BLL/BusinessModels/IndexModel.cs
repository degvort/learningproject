﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LearningProject.BLL.BusinessModels
{
    public class IndexModel<T>
    {
        public IEnumerable<T> Data { get; set; }
        public PageModel PageModel { get; set; }
    }
}
