﻿using System;
using System.Collections.Generic;
using System.Text;
using LearningProject.BLL.DTO;
using LearningProject.DAL.Entities;


namespace LearningProject.BLL.BusinessModels
{
    public class LoginResponse
    {

        public LoginResponse(User user, string _token)
        {
            var response = new
            {
                id = user.Id,
                username = user.Username,
                password = user.Password,
                role = user.Role,
                token = _token
            };
        }
    }
}
