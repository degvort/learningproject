﻿using System;
using System.Collections.Generic;
using System.Text;
using LearningProject.BLL.DTO;
using LearningProject.DAL.Entities;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace LearningProject.BLL.BusinessModels
{
    public static class JwtToken
    {
        public static string GenerateJsonWebToken(User user, string key, string issuer, string audience)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim (ClaimsIdentity.DefaultRoleClaimType, user.Role)
            };

            var token = new JwtSecurityToken(issuer,
               audience,
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
