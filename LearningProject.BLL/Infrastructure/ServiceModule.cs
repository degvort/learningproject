﻿using LearningProject.DAL.Context;
using LearningProject.DAL.Interfaces;
using LearningProject.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Ninject.Modules;
using System;

namespace LearningProject.BLL.Infrastructure
{
    public class ServiceModule
        : NinjectModule
    {

        ApplicationContext _context;
        public ServiceModule(DbContextOptions<ApplicationContext> context, IServiceCollection serv)
        {
            _context = new ApplicationContext(context);
        }

        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(_context);
        }
    }
}
